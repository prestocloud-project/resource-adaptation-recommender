package gr.iccs.presto.RARecom;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import java.util.concurrent.TimeoutException;

import javax.annotation.PostConstruct;

import org.kie.api.runtime.KieSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

@Service
public class EventLoadingService {

	private static final Logger log = LoggerFactory.getLogger(App.class);
	private static final String DEFAULT_EXCHANGE_NAME = "SDM_OUTPUT_EXCHANGE";
	private static final String DEFAULT_IN_TOPIC = "sdm.situtations";

	@Autowired
	private KieSession kSession;

	public EventLoadingService() {

		log.info("===== Event Loader Service Started ! =====");

	}

	@PostConstruct
	private Channel startEventLoader() throws IOException, TimeoutException {

		ClassLoader loader = Thread.currentThread().getContextClassLoader();
		Properties props = new Properties();
		try (InputStream resourceStream = loader.getResourceAsStream("input.properties")) {
			props.load(resourceStream);
		}
		String inHost = props.getProperty("rqhost", "127.0.0.1");
		String inUser = props.getProperty("rqusername", "rmquser");
		String inPass = props.getProperty("rqpassword", "rmqpass");
		String inExchange = props.getProperty("rqexchange", DEFAULT_EXCHANGE_NAME);
		String inTopic = props.getProperty("rqtopic", DEFAULT_IN_TOPIC);

		ConnectionFactory factory = new ConnectionFactory();
		factory.setUsername(inUser);
		factory.setPassword(inPass);
		factory.setHost(inHost);
		Connection connection = factory.newConnection();
		Channel channel = connection.createChannel();

		channel.exchangeDeclare(inExchange, "topic");
		String queueName = channel.queueDeclare().getQueue();

		channel.queueBind(queueName, inExchange, inTopic);

		log.info(" [*] Waiting for AMQP messages. Exchange=" + inExchange + " Topic=" + inTopic + " host=" + inHost);

		EventLoader consumer = new EventLoader(channel, this.kSession);

		channel.basicConsume(queueName, true, consumer);

		log.info("Event Loader Started Succesfully !");

		return channel;

	}

}
