package gr.iccs.presto.RARecom;

import java.io.IOException;
import java.io.InputStream;
import java.lang.management.ManagementFactory;
import java.lang.management.RuntimeMXBean;
import java.text.NumberFormat;
import java.util.Enumeration;
import java.util.List;
import java.util.Properties;
import org.kie.api.KieServices;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@EnableAutoConfiguration
@SpringBootApplication
public class App {

	private static final Logger log = LoggerFactory.getLogger(App.class);
	// private static final String DEFAULT_EXCHANGE_NAME = "SDM_OUTPUT_EXCHANGE";
	// private static final String DEFAULT_IN_TOPIC = "sdm.situtations";

	//@Autowired
	//private KieSession kSession;
	
	//@Autowired
	//private EventLoaderService eventLoaderSrv;

	// private ContextService contextService;

	public App() {
		super();
		try {
			// kSession = init();
			// this.startEventLoader(kSession);
			App.printSystemStatus();
		} catch (Exception e) {
			e.printStackTrace();
		}
		log.info("======= App started ! ========");
	}

//	@Bean
//	public CommandLineRunner run(final ApplicationContext appContext) {
//		return new CommandLineRunner() {
//			@Override
//			public void run(String... args) throws Exception {
//				//Print loaded Beans (DEBUG)
//				String[] beans = appContext.getBeanDefinitionNames();
//				Arrays.sort(beans);
//				for (String bean : beans) {
//					System.out.println(bean);
//				}
//
//			}
//		};
//	}

	@RequestMapping("/status")
	String home() {
		return ("<h1>Resource Adaptation Recommender (RARecom)</h1>\n" + printSystemStatus()).replaceAll("\n", "<p/>");
	}

	@RequestMapping("/drools/release")
	String getDroolsRelease() {

		KieServices ks = KieServices.Factory.get();
		return "Drools session : " + ks.getRepository().getDefaultReleaseId().toString();
	}

	/*****
	 * Main
	 * 
	 * @param args
	 *            : not used
	 */
	public static final void main(String[] args) {
		SpringApplication.run(App.class, args);

		// ApplicationContext context = new
		// AnnotationConfigApplicationContext(App.class);
		// RuleAdaptationController raController = (RuleAdaptationController)
		// context.getBean(RuleAdaptationController.class);
		// raController.kSession = kSession;
	}

	/***
	 * 
	 * @return String
	 */
	private static String printSystemStatus() {
		Runtime runtime = Runtime.getRuntime();
		final NumberFormat format = NumberFormat.getInstance();
		final long maxMem = runtime.maxMemory();
		final long allocMem = runtime.totalMemory();
		final long freeMem = runtime.freeMemory();
		final long ONE_MB = 1024 * 1024;
		final String mega = " MB";

		String msg = "============ JVM Memory Info ==============";
		msg += "\nFree: " + format.format(freeMem / ONE_MB) + mega;
		msg += "\nAllocated: " + format.format(allocMem / ONE_MB) + mega;
		msg += "\nMax: " + format.format(maxMem / ONE_MB) + mega;
		msg += "\nTotal free: " + format.format((freeMem + (maxMem - allocMem)) / ONE_MB) + mega;
		msg += "\n============================================\n";
		msg += "\n============ JVM Arguments  ==============";
		RuntimeMXBean runtimeMXBean = ManagementFactory.getRuntimeMXBean();
		List<String> jvmArgs = runtimeMXBean.getInputArguments();
		for (String arg : jvmArgs) {
			msg += "\n" + arg;
		}
		msg += "\n============================================\n";
		System.out.println(msg);

		return msg;
	}

	@SuppressWarnings("unchecked")
	@RequestMapping("/config")
	private String printSystemConfiguration() throws IOException {
		String ret = "";

		ClassLoader loader = Thread.currentThread().getContextClassLoader();
		Properties INprops = new Properties();
		try (InputStream resourceStream = loader.getResourceAsStream("output.properties")) {
			INprops.load(resourceStream);
		}

		Enumeration<String> enums = (Enumeration<String>) INprops.propertyNames();
		while (enums.hasMoreElements()) {
			String key = enums.nextElement();
			String value = INprops.getProperty(key);
			ret += key + " : " + value + "\n";
		}

		Properties OUTprops = new Properties();
		try (InputStream resourceStream = loader.getResourceAsStream("output.properties")) {
			OUTprops.load(resourceStream);
		}
		enums = (Enumeration<String>) OUTprops.propertyNames();
		while (enums.hasMoreElements()) {
			String key = enums.nextElement();
			String value = OUTprops.getProperty(key);
			ret += key + " : " + value + "\n";
		}

		return "<pre>" + ret + "</pre>";
	}

}
