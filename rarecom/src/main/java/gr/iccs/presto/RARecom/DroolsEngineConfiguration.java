package gr.iccs.presto.RARecom;

import java.io.InputStream;
import java.util.Date;
import java.util.Properties;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import org.kie.api.KieServices;
import org.kie.api.runtime.KieContainer;
import org.kie.api.runtime.KieSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class DroolsEngineConfiguration {

	private static final Logger log = LoggerFactory.getLogger(App.class);
	
	public DroolsEngineConfiguration() {
		log.info("==== Drools engine configuration  ====");
	}
	
	@Bean
	public KieSession init() {
		log.info("===== INITIALIZING DROOLS ENGINE");
		try {
			//printSystemStatus();
			// load up the knowledge base
			KieServices ks = KieServices.Factory.get();
			KieContainer kContainer = ks.getKieClasspathContainer();
			final KieSession kSession = kContainer.newKieSession("ksession-rules");

			ClassLoader loader = Thread.currentThread().getContextClassLoader();
			Properties OUTprops = new Properties();
			try (InputStream resourceStream = loader.getResourceAsStream("output.properties")) {
				OUTprops.load(resourceStream);
			}
			String apHost = OUTprops.getProperty("host", "127.0.0.1");
			String apUser = OUTprops.getProperty("username", "rmquser");
			String apPass = OUTprops.getProperty("password", "rmqpass");
			String apExchange = OUTprops.getProperty("exchange", "RARecom-ACTIONS");

			Properties INprops = new Properties();
			try (InputStream resourceStream = loader.getResourceAsStream("input.properties")) {
				INprops.load(resourceStream);
			}
			String esHost = INprops.getProperty("eshost"); // , "127.0.0.1");
			String esPort = INprops.getProperty("esport"); // , "9200");

			kSession.setGlobal("ap", new ActionPubInterface(apHost, apUser, apPass, apExchange));
			ContextServiceProxy contextService = new ContextServiceProxy(esHost, esPort);
			kSession.setGlobal("ctx", contextService);

			// Use with instead of fireAll
			ExecutorService thread = Executors.newSingleThreadExecutor();
			final Future fireUntilHaltResult = thread.submit(new Runnable() {
				@Override
				public void run() {
					while (true) {
						log.info("^^^^ RULE ENGINE (RE)START @" + new Date());
						try {
							kSession.fireUntilHalt();
						} catch (Exception e) {
							log.info("fireUntilHalt EXCEPTION !!! " + e.getMessage().toString());
							// e.printStackTrace();
						}
					}
				}
			});
			log.info("=====    DROOLS ENGINE CREATED   ====");
			log.info("" + fireUntilHaltResult);
			return kSession;
		} catch (Throwable t) {
			t.printStackTrace();
			log.info("=====  DROOLS ENGINE NOT CREATED ====");
			return null;
		}
		
	}

}
