package gr.iccs.presto.RARecom;

import java.io.IOException;
import java.io.InputStream;
import java.util.Collections;
import java.util.Map;
import java.util.Properties;

import org.apache.http.HttpEntity;
import org.apache.http.HttpHost;
import org.apache.http.entity.ContentType;
import org.apache.http.util.EntityUtils;
import org.elasticsearch.client.Response;
import org.elasticsearch.client.RestClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;
import com.google.gson.JsonObject;

public class ContextServiceProxy {

	private static final Logger log = LoggerFactory.getLogger(App.class);
	private RestClient restClient;
	private Properties contextMappings;
	
	/**
	 * 
	 * @param host : ElasticSearch hostname or IP
	 * @param esPort : ElasticSearch port number
	 */
	public ContextServiceProxy(String host,String esPort) {
		int _esPort = Integer.parseInt(esPort);
		//tag::rest-client-init
        this.restClient = RestClient.builder(new HttpHost(host, _esPort, "http")).build();
        //end::rest-client-init
        
        loadContextMappings();
        
		// this.restClient.close(); // JVM will close the rest client on shutdown
        System.out.println(" [*] [CTX] Context Service Started ! " + host+":"+esPort);        	
	}
	
	private String loadContextMappings() {
		String ret = "FAILED";
		ClassLoader loader = Thread.currentThread().getContextClassLoader();
		contextMappings = new Properties();
		try (InputStream resourceStream = loader.getResourceAsStream("context-mappings.properties")) {
			contextMappings.load(resourceStream);
			ret = "OK";
		} catch (Exception e1) {
			log.debug("loadContextMappings EXCEPTION ! ");
			e1.printStackTrace();
		}				
		return ret;
	}
	
	/***
	 * 
	 * @param conditionName : a condition declared in context-mappings.properties (property name)
	 * @return
	 */
	public JsonObject evaluateContextCondition(String conditionName) {
		
		System.out.println(" [*] [CTX] Executing query with name : " + conditionName);
		
		String jsonString = this.contextMappings.getProperty(conditionName);
		
		System.out.println(" [*] [CTX] Executing query : " + jsonString);
		
		HttpEntity entity = new org.apache.http.nio.entity.NStringEntity(jsonString, ContentType.APPLICATION_JSON);
		Map<String, String> params = Collections.emptyMap();
        try {        	
			params = Collections.singletonMap("pretty", "true");			
			entity = new org.apache.http.nio.entity.NStringEntity(jsonString, ContentType.APPLICATION_JSON);
			Response response = restClient.performRequest("GET", "/_all/_search", params, entity);
			//or for the above qry just : GET /_all/_search?q=dst_ip:216.58.207.46			
			String responseJson = EntityUtils.toString(response.getEntity()); //JSON from qry
			//System.out.println(" [*] [CTX]" + conditionName + " -> " + response.toString());
			JsonObject json = new Gson().fromJson(responseJson, JsonObject.class);
			return json;
			
		} catch (IOException e) {
			// Print exception
			e.printStackTrace();
			System.out.println(e);
			return null;
		}	
	}
	
	/***
	 * 
	 * @param conditionName : get the query string for a specific condition declared in context-mappings.properties
	 * @return
	 */
	public String getContextConditionQuery(String conditionName) {
		return this.contextMappings.getProperty(conditionName);
	}
	
	/***
	 * 
	 * @param jsonString : evaluate an Elasticsearch query to /_all/_search
	 * @return
	 */
    public JsonObject evaluateContextQuery(String jsonString) {
						
		System.out.println(" [*] [CTX] Executing query : " + jsonString);
		
		HttpEntity entity = new org.apache.http.nio.entity.NStringEntity(jsonString, ContentType.APPLICATION_JSON);
		Map<String, String> params = Collections.emptyMap();
        try {        	
			params = Collections.singletonMap("pretty", "true");			
			entity = new org.apache.http.nio.entity.NStringEntity(jsonString, ContentType.APPLICATION_JSON);
			Response response = restClient.performRequest("GET", "/_all/_search", params, entity);
			//or for the above qry just : GET /_all/_search?q=dst_ip:216.58.207.46			
			String responseJson = EntityUtils.toString(response.getEntity()); //JSON from qry
			//System.out.println(" [*]  -> " + response.toString());
			JsonObject json = new Gson().fromJson(responseJson, JsonObject.class);
			return json;
			
		} catch (IOException e) {
			// Print exception
			e.printStackTrace();
			System.out.println(e);
			return null;
		}	
	}

}
