package gr.iccs.presto.RARecom;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

import org.kie.api.runtime.KieSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.google.gson.JsonSyntaxException;
import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.DefaultConsumer;
import com.rabbitmq.client.Envelope;


public class EventLoader extends DefaultConsumer {
	
	private static final Logger log = LoggerFactory.getLogger(App.class);	
	
	private KieSession kSession;
	
	private Gson _gson;

	public EventLoader(Channel channel, KieSession kSession) throws IOException, TimeoutException {
		super(channel);
		this.kSession = kSession;
		this._gson = new GsonBuilder().create();		
		log.info("*** Event Loader created ! ***");
	}
		
	
	
	@Override
    public void handleDelivery(String consumerTag, Envelope envelope,
                               AMQP.BasicProperties properties, byte[] body) throws IOException {
		JsonObject json;
		String message = new String(body, "UTF-8");
      //System.out.println(" [x] AMQP EVENT FROM TOPIC '" + envelope.getRoutingKey() + "'\n:'" + message + "'");
      //System.out.println(" [x] AMQP EVENT FROM TOPIC '" + envelope.getRoutingKey() );
      if (this.kSession != null) {
      	//insert event into Drools Fusion (CEP)
    	
    	  try {
			json = this._gson.fromJson(message, JsonObject.class);
		} catch (JsonSyntaxException e) {
			// do nothing
			e.printStackTrace();
			return;
		}
    	//System.out.println(json);
  		
  		//try {
			this.kSession.getEntryPoint("INPUT").insert(json);
			//long facts = this.kSession.getFactCount();
			//System.out.println(" [x] FACTS in KB="+facts);
			//this._kSession.fireAllRules(); //comment out if fireUntilHalt mode is used
		//} catch (Exception e) {
			// TODO Auto-generated catch block
		//	System.out.println("Error processing message :" + json);
		//	e.printStackTrace();
		//}
      	log.info("EVENT INSERTED :" + json);
      } else {
    	 log.info("\nDROOLS KIESESSION IS NULL\nFAILED TO LOAD EVENT : "+message);
      }
    }
}
