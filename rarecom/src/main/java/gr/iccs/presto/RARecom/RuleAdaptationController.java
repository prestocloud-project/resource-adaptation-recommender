package gr.iccs.presto.RARecom;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.drools.core.impl.KnowledgeBaseImpl;
import org.kie.api.KieBase;
import org.kie.api.definition.KiePackage;
import org.kie.api.definition.rule.Rule;
import org.kie.api.io.ResourceType;
import org.kie.api.runtime.KieSession;
import org.kie.internal.builder.KnowledgeBuilder;
import org.kie.internal.builder.KnowledgeBuilderFactory;
import org.kie.internal.io.ResourceFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class RuleAdaptationController {
	private static final Logger log = LoggerFactory.getLogger(App.class);
	@Autowired
	public KieSession kSession;
			
	public RuleAdaptationController() {
		log.info("RuleAdaptationController created !");
	}
	
	@RequestMapping("/facts/count")
	String countFacts() {
		return "FACTS in KB = " + this.kSession.getFactCount();
	}
	
	@RequestMapping(path = "/rules/list", produces=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Object> getDroolsRulesPerPackage() {
		
		Collection<KiePackage> packages = this.kSession.getKieBase().getKiePackages();
		HashMap<String, Object> map = new HashMap<>();
		for (KiePackage pkg : packages) {
			//ret += "Package " + pkg.getName() + "\n";
			ArrayList<Map> pkgrules = new ArrayList<>();
			Collection<Rule> rules = pkg.getRules();
			for (Rule rule : rules) {
				//ret += "\t" + rule.toString() + "\n";
				HashMap<String, String> rmap = new HashMap<>();
				rmap.put("name", rule.getName());
				rmap.put("namespace", rule.getNamespace());
				pkgrules.add(rmap);
			}
			map.put(pkg.getName(), pkgrules);
		}
		return new ResponseEntity<Object>(map, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/rules/add/{rulePackage}/{ruleName}", method = RequestMethod.POST , consumes="text/plain")
	String addRule(@PathVariable String rulePackage, @PathVariable String ruleName,
			@RequestBody String ruleText) {
		String errors;

		KieBase kbase = this.kSession.getKieBase();
		KnowledgeBuilder kbuilder = KnowledgeBuilderFactory.newKnowledgeBuilder();
		kbuilder = KnowledgeBuilderFactory.newKnowledgeBuilder(kbase);
		kbuilder.add(ResourceFactory.newByteArrayResource(ruleText.getBytes()), ResourceType.DRL);
		// Show errors
		if (kbuilder.hasErrors()) {
			errors = kbuilder.getErrors().toString();
			System.out.println(ruleText);
			System.out.println(errors);
			return "ERROR: " + errors;
		}
		((KnowledgeBaseImpl) this.kSession.getKieBase()).addPackages(kbuilder.getKnowledgePackages());
		return "SUCCESS";
	}
	
	@RequestMapping("/rules/delete/{rulePackage}/{ruleName}")
	String removeRule(@PathVariable String rulePackage, @PathVariable String ruleName) {
		// examples from :
		// https://github.com/kiegroup/drools/tree/7.7.x/drools-compiler/src/test/java/org/drools/compiler/integrationtests
		// KiePackage pkg = this.kSession.getKieBase().getKiePackage(rulePackage);
		try {
			this.kSession.getKieBase().removeRule(rulePackage, ruleName);
		} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return "FAILED: " + e.getMessage();
		}
		return "SUCCESS: removed + " + ruleName;
	}

}
