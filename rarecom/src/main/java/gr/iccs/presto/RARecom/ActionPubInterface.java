package gr.iccs.presto.RARecom;

import com.rabbitmq.client.*;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

public class ActionPubInterface {

	private String EXCHANGE_NAME = "RARecom-ACTIONS";
	private Channel channel;
	private Connection connection;

	/***
	 * 
	 * @param host : RabbitMQ hostname or IP
	 * @param uname : RabbitMQ username
	 * @param pass : RabbitMQ password
	 * @param exchangeName : RabbitMQ exchange name (must have type = topic)
	 * @throws IOException
	 * @throws TimeoutException
	 */
	public ActionPubInterface(String host, String uname, String pass, String exchangeName) throws IOException, TimeoutException {
		super();
		this.EXCHANGE_NAME = exchangeName;
		ConnectionFactory factory = new ConnectionFactory();
		factory.setHost(host);
		factory.setPassword(pass);
		factory.setUsername(uname);
		this.connection = factory.newConnection();
		this.channel = connection.createChannel();
		this.channel.exchangeDeclare(EXCHANGE_NAME, "topic");
	}

	/***
	 * Publishes an event to RabbitMQ
	 * @param topic : RabbitMQ topic
	 * @param message : event payload (JSON String)
	 * @throws Exception
	 */
	public void publish(String topic, String message) throws Exception {

		String routingKey = topic;
		channel.basicPublish(EXCHANGE_NAME, routingKey, null, message.getBytes());
		System.out.println(" [x] AMQP Sent '" + routingKey + "':'" + message + "'");
		// connection.close(); //where and when ?
	}

}
