FROM maven:alpine

COPY ./rarecom /rarecom

WORKDIR /rarecom

RUN mvn clean compile
